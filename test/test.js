/**
 * Created by strrife on 2/23/16.
 */
var assert = require('assert');
var calculator = require('../src/calculator');

describe('Calculator.add', function() {
    it('should add zero numbers', function () {
        assert.equal(calculator.add(''), 0)
    });

    it('should add one number', function () {
        assert.equal(calculator.add('5'), 5)
    });

    it('should add two number', function () {
        assert.equal(calculator.add('5,8'), 13)
    });

    it('should add unknown amount of numbers', function () {
        assert.equal(calculator.add('1,2,3,4'), 10)
    });

    it('should mark input as invalid', function () {
        assert.equal(calculator.add('1,'), null)
    });

    it('should support newlines as delimiters', function () {
        assert.equal(calculator.add('1,2\n3'), 6)
    });

    it('should mark input as invalid', function () {
        assert.equal(calculator.add('1,\n'), null)
    });

    it('should namdle custom delimiters', function () {
        assert.equal(calculator.add('//;\n1;2'), 3)
    });

    it('should be ok with regex delims', function () {
        assert.equal(calculator.add('//?\n1?2'), 3)
    });

    it('should support old delimiters when adding new one', function () {
        assert.equal(calculator.add('//?\n1,2'), 3)
    });

    it('should throw an exception on negatives', function () {
        assert.throws(calculator.add.bind(calculator, '1,-2'))
    });

    it('should skip >1000', function () {
        assert.equal(calculator.add('1,1001'), 1);
    });

    it('should be ok with long delims', function () {
        assert.equal(calculator.add('//[***]\n1***2'), 3)
    });

    it('should be ok with multiple long delims', function () {
        assert.equal(calculator.add('//[*][%%]\n1*2%%3'), 6)
    });
});