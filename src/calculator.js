/**
 * Created by strrife on 2/23/16.
 */
"use strict";

const DEFAULT_DELIMITER = ',|\n';
const MAX_NUMBER = 1000;
const DELIMITER_DETECTION_REGEX = '^//(.+)\n';

RegExp.escape = function(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

var processInput = function(string){
    var delimiterCandidate = string.match(DELIMITER_DETECTION_REGEX);
    if(delimiterCandidate) {
        string = string.substr(delimiterCandidate[0].length);
        delimiterCandidate = delimiterCandidate[1];
    }

    var delimiter = DEFAULT_DELIMITER;
    if(delimiterCandidate && /^\[.+]$/.test(delimiterCandidate)){
        delimiter += "|" + delimiterCandidate
                .substr(1, delimiterCandidate.length - 2)
                .split(/]\[/)
                .map(RegExp.escape)
                .join('|');
    } else if(delimiterCandidate){
        delimiter += "|" + RegExp.escape(delimiterCandidate)
    }


    return { string, delimiter };
};

var checkNegatives = function(numbers){
    var negatives = numbers.filter(x => x < 0);
    if(negatives.length){
        throw { message: "Negatives not allowed", data: negatives};
    }
};

module.exports = {
    add : (string) => {
        var input = processInput(string);
        if(input.string === '' || new RegExp('^(?:-?\\d+(?:' + input.delimiter + '))*-?\\d+$').test(input.string)) {
            var numbers = input.string.split(new RegExp(input.delimiter)).map(e => Number(e));
            checkNegatives(numbers);
            return numbers.filter(x => x <= MAX_NUMBER).reduce((sum, el) => sum + Number(el), 0);
        } else
            return null;
    }
};